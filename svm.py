# -*- coding: utf-8 -*-

from input import *
from sklearn import svm



def get_hash(lst):
	hash_ = {}
	for i in range(len(lst)):
		hash_[lst[i]] = i	
	return hash_

addr = "./FILE/"
d = Data()

print " === start SVM ==="
data = d.input_file(addr)


print "# read data complete"


list_class = d.get_class_list()
c_hash = get_hash(list_class)

#print A["edu"][0]["วัน"]

words = d.df.keys()
w_hash = get_hash(words)
n_words = len(words)
print "There is" , n_words, "word type in Corpus"

X = []
Y = []
for c in list_class:
	for doc in data[c]:
		freq = [0 for i in range(n_words)]
		for w in doc.keys():
			freq[w_hash[w]] = doc[w]
		X.append(freq)
		Y.append(c_hash[c])

#X = [[0, 0], [1, 1],[10,100],[5,9]]
#y = [0, 1,2,3]
print "# initail data complete"
clf = svm.SVC()
clf.fit(X, Y)  

print "# SVM model complete"
print "# Predict"
while(1):
	print "input: "
	path = raw_input()
	try:
		doc = d.query(path)
		freq = [0 for i in range(n_words)]
		for w in doc.keys():
			if w_hash.has_key(w):
				freq[w_hash[w]] = doc[w]

		doc_class = clf.predict([freq])
		print "result: ",list_class[doc_class]

	except Exception as e:
		print e

