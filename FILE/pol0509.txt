***
ทรท.-แฮร์รอดตั้ง
มูลนิธิไทยพึ่งไทย
"ช่วยคนว่างงาน"

ทรท.-แฮร์รอด ตั้งมูลนิธิไทยพึ่งไทย "ช่วยคนว่างงาน"

แกนนำพรรคไทยรักไทย ร่วมมือแฮร์รอดห้างดัง จัดตั้งมูลนิธิไทยพึ่งไทย ฝึกอาชีพให้กับคนว่างงาน 
แม่บ้านนักศึกษา และคนตกงาน รวมทั้งจัดหาตลาดจำหน่ายตามห้างสรรพสินค้าต่างๆ หลายแห่ง 
พร้อมกันนี้ นายโมฮัมหมัด ยังมอบทุนหมุนเวียนให้กับมูลนิธิและนำสินค้าไปจำหน่ายยังประเทศอังกฤษอีกด้วย 


นางสุดารัตน์ เกยุราพันธุ์ อดีตส.ส.กทม. กรรมการบริหารพรรคไทยรักไทย เปิดเผยว่า 
ขณะนี้เขาได้ตั้งมูลนิธิไทยพึ่งไทยขึ้นมา โดยมีวัตถุประสงค์เพื่อฝึกฝนอาชีพให้กับคนว่างงาน 
แม่บ้าน นักศึกษา คนตกงาน ทั้งนี้มูลนิธิจะให้ความช่วยเหลือในด้านการตลาดอีกด้วย 


นางสุดารัตน์ กล่าวอีกว่า มูลนิธิไทยพึ่งไทย ยังได้รับความร่วมมือจากห้างสรรพสินค้าต่างๆหลายแห่ง 
รวมทั้งห้างสรรพสินค้าแฮร์รอดจากประเทศอังกฤษ ก็ยินดีรับสินค้าจากมูลนิธิไปจำหน่าย 
ขณะเดียวกัน นายโมฮัมหมัด อัลฟาเอด เจ้าของห้างดังกล่าวยังมอบทุนหมุนเวียนให้กับมูลนิธิด้วย 
โดยจะรับมอบทุนในวันที่ 7 เมษายนนี้เวลา 11.00 น. ณ ห้องประชุม ชั้น 9 อาคารชินวัตร 
3 

KT Internet Dept


Home I The Nation I EntNet I เนชั่นสุดสัปดาห์ I วิทยุเนชั่น
About Us I FAQ I Site Map I Contact Us I Policy
