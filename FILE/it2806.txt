เลมอนออนไลน์ ทุ่ม300ล.ทำระบบ รับอี-คอมเมิร์ซ

วันศุกร์ที่ 28 เมษายน พ.ศ.2543
เลมอนออนไลน์ ทุ่ม300ล.ทำระบบ รับอี-คอมเมิร์ซ

เลมอนออนไลน์ทุ่ม 300 ล้านบาท พัฒนาระบบ เนื้อหาสู่บริการอี-คอมเมิร์ซ โดยแยกเป็น 
3 เวบไซต์ รวมทั้งสร้างชื่อ "เลมอน" เป็นเวบไซต์เด่นในภูมิภาคนี้

นายชิตชัย นันทภัทร์ กรรมการผู้จัดการ บริษัท ฮัทชิสัน เพจโฟน จำกัด กล่าวว่า 
ปีนี้บริษัทจะให้ความสำคัญต่อการดำเนินธุรกิจอินเทอร์เน็ตมากขึ้น โดยจะใช้งบ 300 
ล้านบาท พัฒนาเนื้อหาในเวบเลมอนออนไลน์, รวมถึงปรับซอฟต์แวร์ และแอพพลิเคชั่น 
ต่างๆ เพื่อรองรับธุรกิจอี-คอมเมิร์ซ

สำหรับเนื้อที่อยู่ในรูปเวบไซต์ดอทคอม ประกอบด้วย ชอป4567ดอทคอม (shop4567. com) 
และไทยฟาสต์เวย์ดอทคอม ( thaifastway.com) ที่คาดว่า จะเปิดให้บริการได้ราวเดือนมิถุนายนนี้ 
ซึ่งส่วนของชอป 4567ดอทคอมนั้น จะมีรูปแบบเสมือนห้างสรรพสินค้า ที่ทำการตลาดในลักษณะธุรกิจต่อผู้บริโภค 
(บีทูซี) และผู้บริโภคต่อผู้บริโภค (ซีทูซี) ซึ่งผู้ต้องการขายสินค้าสามารถเข้ามาฝากขายในเวบไซต์นี้ได้ 
โดยลูกค้าจะสั่งซื้อสินค้าได้ 2 ระบบคือ ผ่านระบบออนไลน์ และสั่งซื้อผ่านโทรศัพท์จากศูนย์บริการโทรศัพท์ 
(คอลล์เซ็นเตอร์) ของฮัทชิสัน ที่ปัจจุบันมียอดการสั่งซื้อสินค้าทางโทรศัพท์ประมาณเดือนละ 
2 ล้านบาท

อย่างไรก็ตาม สินค้าที่จะนำมาวางบนเวบไซต์นี้ จะเป็นสินค้าพรีเมี่ยม และสินค้าที่ระลึกโดยมีราคาไม่สูงมากนัก 
และไม่วางขายในท้องตลาดมากนัก เพื่อกระตุ้นให้ผู้ใช้บริการซื้อขายผ่านระบบนี้ 
โดยการเรียกเก็บเงินจะดำเนินการได้ผ่านระบบบัตรเครดิต หรือลูกค้าจะชำระเงินเมื่อได้รับสินค้าแล้ว

ส่วนไทยฟาสต์เวย์ดอทคอม (thaifastway.com) เป็นการดำเนินธุรกิจกับลูกค้าองค์กรแบบธุรกิจต่อธุรกิจ 
รวมถึงล็อกซ์นิวส์ดอทคอม (loxnews.com) จะเป็นเวบไซต์ที่ให้บริการข่าวสารเฉพาะด้าน 
สำหรับลูกค้าที่ต้องการข้อมูลเบื้องลึกบางเรื่อง เช่น การรายงานซื้อขายหุ้นจากตลาดหลักทรัพย์ 


ดันชื่อเลมอนเป็นเวบไซต์เด่น

สำหรับเลมอนออนไลน์ ที่มีผู้ให้บริการอยู่ในประเทศต่างๆ ของภูมิภาคนี้ ประกอบด้วย 
สิงคโปร์ มาเลเซีย อินเดีย และออสเตรเลีย โดยนโยบายนั้น จะเน้นสร้างชื่อให้เป็นเวบไซต์ของภูมิภาค 
ซึ่งผู้ให้บริการในแต่ละประเทศจะแบ่งปันการใช้เนื้อหาในของแต่ละสาขา ส่วนในไทยจะพัฒนาเวบไซต์ออกเป็น 
2 ภาษา ทั้งภาษาไทย และภาษาอังกฤษ

ส่วนแนวโน้มผู้ใช้บริการอินเทอร์เน็ตจากการทำวิจัยของบริษัทพบว่า มีผู้ใช้บริการในประเทศไทยปีที่แล้วประมาณ 
8 แสนคน คาดว่าปีนี้ จะมีผู้ใช้อินเทอร์เน็ตเพิ่มขึ้นเป็น 1.5 ล้านคน หรือเกือบ 
100% 

Krungthep Turakij Newspaper
