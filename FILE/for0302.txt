'โรคปาก-เท้าเปื่อย'ในโสมขาว ขยายวงระบาดสู่ตะวันตกเฉียงใต้

วันจันทร์ที่ 3เมษายน พ.ศ.2543
'โรคปาก-เท้าเปื่อย'ในโสมขาว ขยายวงระบาดสู่ตะวันตกเฉียงใต้

โซล - โรคปากและเท้าเปื่อยในเกาหลีใต้ เริ่มขยายวงไปสู่ฟาร์มปศุสัตว์ทางชายฝั่งทะเลตะวันตกเฉียงใต้ของประเทศ 
ส่งผลให้ทางการต้องเร่งหามาตรการฉุกเฉินเพื่อป้องกันการแพร่ระบาดของโรค

กระทรวงเกษตร เกาหลีใต้ เปิดเผยวานนี้ (2 เม.ย.) ว่า เจ้าหน้าที่สถานกักสัตว์ได้ตรวจพบการระบาดของแผลพุพอง 
ซึ่งเป็นสาเหตุของโรคปากและเท้าเปื่อย ครั้งที่ 2 ในหมู่บ้านที่ทำฟาร์มปศุสัตว์แห่งหนึ่ง 
ในเมืองจางหยาง ทางฝั่งตะวันตกเฉียงใต้ของประเทศ โดยกระทรวงได้คาดการณ์ไว้ล่วงหน้าแล้วว่า 
พื้นที่แห่งนี้ ซึ่งอยู่ห่างจากเมืองปันจู ซึ่งมีการตรวจพบการแพร่ระบาดของโรคเป็นครั้งแรกเมื่อสัปดาห์ที่แล้ว 
จะอยู่ในเขตการระบาดของโรคนี้ด้วย

นายคิม ดอง เคียน รัฐมนตรีช่วยว่าการกระทรวงเกษตร เกาหลีใต้ ระบุว่า ต้องหามาตรการที่เข้มงวดเพื่อจัดการกับโรคนี้ 
พร้อมเรียกร้องให้มีมาตรการป้องกันการแพร่ระบาดในระดับจังหวัด และเมืองต่างๆ โดยเฉพาะชายฝั่งทางตะวันตกของประเทศ 
เพื่อไม่ให้โรคร้ายนี้ขยายวงกว้างไปทั่วประเทศ

ส่วนหนึ่งของมาตรการในการป้องกันโรคปาก และเท้าเปื่อยนี้ รวมถึง การให้เจ้าหน้าที่ตำรวจตั้งเขตกักกันสัตว์ 
ภายในรัศมี 10 กิโลเมตร ของพื้นที่ที่มีการแพร่ระบาด เพื่อฉีดวัคซีนป้องกันให้กับสัตว์กีบ 
ทั้งช่วงเช้าวานนี้ สำนักงานสัตวแพทย์ และกักกันสัตว์แห่งชาติ (เอ็นวีคิวเอส) 
ของเกาหลีใต้ พร้อมด้วยเจ้าหน้าที่ตำรวจ และข้าราชการในเขตต่างๆ ต้องช่วยกันฆ่า 
และเผาฝูงวัวจำนวน 93 ตัว ในเมืองฮ่องซง เพื่อควบคุมการระบาดของโรค

ทั้งนี้ การเปิดเผยข้างต้น นับเป็นการยืนยันอย่างเป็นทางการถึงการระบาดของโรคดังกล่าวเป็นครั้งแรกในเอเชียนับแต่โรคนี้ระบาดในหมู่สุกรของไต้หวันเมื่อ 
3 ปีที่แล้ว โดยสาเหตุของโรคปาก และเท้าเปื่อยนั้น มาจากเชื้อไวรัส ที่สามารถแพร่กระจายผ่านทางอากาศ 
และอาหารที่ให้สัตว์ได้อย่างรวดเร็ว และมีแนวโน้มที่จะทำให้สัตว์ที่ติดเชื้อตายสูงถึง 
55%

Krungthep Turakij Newspaper
