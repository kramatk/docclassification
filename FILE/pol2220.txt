กฟผ.รับสมัครผู้ว่า-พนง.ขอคนรู้จริงไร้เส้น

วันเสาร์ ที่ 22 เมษายน พ.ศ.2543
กฟผ.รับสมัครผู้ว่า-พนง.ขอคนรู้จริงไร้เส้น

บอร์ด กฟผ.ใช้วิธีเปิดรับสมัครผู้สนใจนั่งเก้าอี้ผู้ว่าฯ ตั้งศิววงศ์ ประธานสรรหา 
สพร.ขานรับใครก็ได้ไม่ทุจริต

นายบุญส่ง พ่อค้าทอง คณะกรรมการการไฟฟ้าฝ่ายผลิตแห่งประเทศไทย (กฟผ.) เปิดเผยถึงการแต่งตั้งรองผู้ว่าการ 
กฟผ.ที่จะว่างลง 2 ตำแหน่ง ว่า ขณะนี้ยังไม่ได้อนุมัติแต่งตั้งใครขึ้นมาดำรงตำแหน่งแทน 
โดยให้เป็นหน้าที่ของคณะกรรมการสรรหา ซึ่งมีนายศิววงศ์ จังคศิริ ประธานคณะกรรมการ 
กฟผ.เป็นประธาน ให้พิจารณาควบคู่ไปกับการพิจารณาสรรหาผู้ว่าการ กฟผ.คนใหม่แทน 
นายวีระวัฒน์ ชลายน ที่จะเกษียณในปีนี้ ซึ่งจะมีการเปิดรับสมัครทั่วไป เหมือนกับตำแหน่งผู้อำนวยการองค์การสื่อสารมวลชนแห่งประเทศไทย 
(อ.ส.ม.ท.)

นายสภาพร มณีรัตน์ นายกสมาคมพนักงานรัฐวิสาหกิจ (สพร.กฟผ.) กล่าวว่า พร้อมที่จะรับบุคคลภายนอกเข้ามาเป็นผู้ว่าการ 
กฟผ.แต่ขอให้เป็นผู้ที่มีความรู้ความสามารถชำนาญเฉพาะด้านการทำธุรกิจไฟฟ้า ไม่ใช่ใครก็ได้ 
และต้องเป็นผู้ที่มีความซื่อสัตย์โปร่งใส ปราศจากการเมือง 

Krungthep Turakij Newspaper
