9รุ่นในโอลิมปิก... ความสุดยอดของมวยไทย

วันศุกร์ที่ 7 เมษายน พ.ศ.2543
9รุ่นในโอลิมปิก... ความสุดยอดของมวยไทย

อนุชิต กุลวานิช

คงไม่มีอะไรมากมายเกินกว่านี้แล้ว สำหรับความสำเร็จชนิดทะลุเป้าของทีมนักชกไทย 
ที่สามารถผ่านเข้าสู่การแข่งขันกีฬาโอลิมปิก 2000 หรือ ซิดนีย์เกมส์ ได้พร้อมหน้าพร้อมตากันถึง 
9 รุ่น

ถือเป็นความสำเร็จแบบ 100 เปอร์เซ็นต์เต็ม แม้ว่าจะต้องรอกันถึง 3 เลค เพราะก่อนหน้านี้ใน 
2 เลคแรกที่อุซเบกิสถาน และเกาหลีใต้ ทีมไทยได้มาเพียง 5 รุ่น ด้วยเหตุผลต่างๆ 
นานา ไม่ว่าแพ้เขาจริง, แพ้แบบไม่น่าแพ้ หรือแม้แต่ชนะแล้วแต่ถูกกรรมการโกงปรับให้เป็นฝ่ายแพ้น่าตาเฉย

แต่สุดท้ายทีมขุนพลเสื้อกล้ามไทย ก็อาศัยความเป็นเจ้าบ้านเก็บสแปร์ได้ครบในอีก 
4 รุ่นที่เหลือ สร้างประวัติศาสตร์เข้ารอบเรียงตามน้ำหนักกันถึง 9 รุ่นรวด จะเป็นรองชาติในเอเชียก็คงมีเพียง 
อุซเบกิสถาน กับ เกาหลีใต้ ที่ส่งนักชกครบทั้ง 12 รุ่นเท่านั้น

ต้องขอย้ำคำว่า "เจ้าบ้าน" อีกครั้ง เพราะเท่าที่ดูแม้ว่านักชกไทยทั้ง 4 คนจะชนะคู่แข่งได้อย่างไร้ข้อกังขา 
คว้าชัยด้วยฝีมือล้วนๆ แต่ที่น่าสงสัย ก็คือ คะแนนที่ออกมามันมากมายเกินความจริงไปหน่อย 
แต่ของแบบนี้ก็ว่ากันไม่ได้ เพราะเวลาเราออกไปชกนอกบ้านก็เคยโดยโกงแบบหน้าด้านๆ 
มาแล้ว ครั้นมาชกในบ้านจะถูกโกงคาถิ่น หรือกรรมการไม่ช่วยเลยมันก็ดูจะน่าเกลียดไปหน่อย

ก่อนจะผ่านตรงนี้ไป ก็ต้องขอบันทึกรายชื่อของนักชกชุดประวัติศาสตร์ไว้ดังนี้ขอรับ 


สุบรรณ พันโนน รุ่นไลท์ฟลายเวท, วิจารณ์ พลฤทธิ์ รุ่นฟลายเวท, โชติพัฒน์ (สนธยา) 
วงศ์ประเทศ รุ่นแบนตัมเวท, สมรักษ์ คำสิงห์ รุ่นเฟเธอร์เวท, พงษ์สิทธิ์ เวียงวิเศษ 
รุ่นไลท์เวท, พงษ์ศักดิ์ เหรียญทวนทอง รุ่นไลท์เวทเตอร์เวท, ภาคภูมิ แจ้งโพธินาค 
รุ่นเวลเตอร์เวท, พรชัย ทองบุราณ รุ่นไลท์มิดเดิลเวท และ สมชาย ฉิมรัมย์ รุ่นมิดเดิลเวท

ความสำเร็จของทีมนักชกไทยครั้งนี้ คงพิสูจน์ได้ชัดเจนถึงการทำงานของสมาคมมวยสากลสมัครเล่น 
ที่มี พล.อ.สำเภา ชูศรี เป็นนายกสมาคมได้ว่า มีการจัดระบบการทำงานที่ดีเยี่ยมเพียงใด 
และคงเป็นการตอกย้ำกันอีกครั้งว่า ความหวังเหรียญทองของทัพนักกีฬาไทยในโอลิมปิกเกมส์ครั้งนี้ 
คงอยู่ที่กีฬา "มวย" อีกเช่นเคย

เขียนแบบนี้ก็เพื่อเตือนให้สมาคมกีฬาอื่นไม่ต้องอิจฉาตาร้อน หากในการจัดสรรงบประมาณในการเตรียมนักกีฬาแล้วสมาคมมวยสากลสมัครเล่นจะได้รับเงินมากกว่าสมาคมอื่นๆ 
อย่างที่เคยมีมาแล้ว

ในเมื่อมีผลงาน มีความหวังก็ควรจะได้ "สิทธิพิเศษ" ไม่ใช่ว่าไปแล้วแพ้เค้ายันเตแล้วยังหวังแบ่งเท่ากัน 
แบบนี้ถือว่าไม่ยุติธรรมขอรับ

Krungthep Turakij Newspaper
