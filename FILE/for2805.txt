สำนักข่าวยูโกสลาเวียอ้าง 'บิน ลาเดน'อยู่ในโคโซโว

วันศุกร์ที่ 28 เมษายน พ.ศ.2543
สำนักข่าวยูโกสลาเวียอ้าง 'บิน ลาเดน'อยู่ในโคโซโว

เบลเกรด - สำนักข่าวของทางการยูโกสลาเวีย อ้าง "ออสมา บิน ลาเดน" มหาเศรษฐีซาอุฯ 
ซึ่งถูกสหรัฐขึ้นบัญชีดำ ได้ที่ลี้ภัยใหม่ในโคโซโว หลังกบดานในอัฟกานิสถานมาหลายปี

สำนักข่าวตันยุกของทางการยูโกสลาเวีย รายงานวานนี้ว่า นายออสมา บิน ลาเดน มหาเศรษฐีชาวซาอุดีอาระเบีย 
ซึ่งเป็นที่ต้องการตัวของสหรัฐหลังถูกกล่าวหาว่าอยู่เบื้องหลังการก่อการร้าย กำลังอยู่ในโคโซโว

"หลังจากกบดานอยู่ในอัฟกานิสถานมาหลายปี นายบิน ลาเดน ได้พบแหล่งลี้ภัยแห่งใหม่ในคาบสมุทรบอลข่าน 
หรือให้ชัดเจนลงไปคือที่จังหวัดโคโซโว อันเป็นรังของการก่อการร้ายในยุโรป"

ตันยุกรายงานว่า นายบิน ลาเดน ซึ่งสำนักข่าวแห่งนี้มักเรียกว่า "คนคลั่งก่อการร้ายและหลงใหลอิสลาม" 
ได้เดินทางจากแอลเบเนียเข้าสู่โคโซโว หลังจากรวบรวมกลุ่มนักรบอิสลามได้ 500 คนจากบริเวณภาคตะวันออก 
ในแนวจังหวัดคอร์ซีและโปกราเดก เพื่อเข้าไปก่อการร้ายในโคโซโว

นอกจากนั้น เขายังมีแผนก่อการร้ายในจังหวัดเซอร์เบีย ทางตอนใต้ ซึ่งอยู่บริเวณชายแดนติดกับโคโซโว 
รวมถึงในจังหวัดพรีซีโว, เมดเวดจา และบูยาโนแวก

ความตึงเครียดในโคโซโวได้เพิ่มขึ้นในช่วง 10 วันที่ผ่านมา โดยมีเหตุปะทะกันบ่อยครั้ง 
ขณะที่ชนกลุ่มน้อยเชื้อสายแอลเบเนียก็ยื่นข้อเรียกร้องต่างๆ มากขึ้น โดยจังหวัดโคโซโวประกอบด้วยชนเชื้อสายแอลเบเนียประมาณ 
70,000 คน หลายคนร้องเรียนว่าถูกกดขี่จากตำรวจเซอร์เบีย

ทั้งนี้ สหรัฐขึ้นบัญชีดำบิน ลาเดน ฐานเกี่ยวข้องกับการวางระเบิดสถานทูตสหรัฐในเคนยาและแทนซาเนียเมื่อปี 
2541 ซึ่งทำให้มีผู้เสียชีวิต 224 คน 

Krungthep Turakij Newspaper
