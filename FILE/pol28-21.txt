ต่อ

ต่อ

ด้านพล.อ.สุรยุทธ์ จุลานนท์ ผู้บัญชาการทหารบก กล่าวถึงบ่อนการพนันที่อยู่ตามแนวชายแดนว่าในระยะยาวจะมีผลกระทบต่อความมั่นคงของชาติเพราะจะทำให้จำนวนคนข้ามไปข้ามมามากขึ้น 
ทางไทยคงไปห้ามไม่ให้มีการเปิดบ่อนในฝั่งกัมพูชาไม่ได้ ซึ่งเมื่อคนไทยข้ามไปเล่นก็จะสร้างความเจริญในพื้นที่รวมถึงรายได้ที่มากขึ้น 
แต่ก็จะมีผลกระทบในเรื่องของขโมยที่มากขึ้น มีการลักลอบค้าขายสินค้าตามแนวชายแดน 
และไม่ใช่หน้าที่ทหารที่จะเข้าไปดูแลความเรียบร้อย 


การเมืองเรื่องส่วนรวม คิดอย่างไร รู้สึกอย่างไร ช่วยกันแสดงความเห็น สร้างสรรค์ 
ระบอบประชาธิปไตย 

KT Internet Dept


Home I The Nation I EntNet I เนชั่นสุดสัปดาห์ I วิทยุเนชั่น
About Us I FAQ I Site Map I Contact Us I Policy
