แผนยกเครื่องภาคการเงินอินโดนีเซีย'สะดุด'

วันจันทร์ที่ 3เมษายน พ.ศ.2543
แผนยกเครื่องภาคการเงินอินโดนีเซีย'สะดุด'


• หลังศาลตัดสิน'โมฆะ'เทคโอเวอร์แบงก์บาหลี

พัสพา มาดานิ เกรนน์ แม็คคาร์ธีย์

จาการ์ตา- แผนปรับโครงสร้างภาคธนาคารของอินโดนีเซียอยู่ในภาวะที่เรียกว่า ถอยหลังเข้าคลองอย่างแท้จริง 
ทันทีที่ศาลจาการ์ตา ตัดสินชี้ขาดให้แผนเข้าครอบครองกิจการแบงก์ บาหลี ของสำนักงานปรับโครงสร้างภาคธนาคารอินโดนีเซีย 
(ไอบีอาร์เอ) เป็นโมฆะ

ไอบีอาร์เอ และแบงก์ อินโดนีเซีย ธนาคารกลางของประเทศ เตรียมยื่นอุทธรณ์คำตัดสินชี้ขาดของศาลเกี่ยวกับเรื่องนี้ในโอกาสต่อไป 
โดยแบงก์ อินโดนีเซีย ยืนยันในแถลงการณ์ว่า การอนุมัติแผนเข้าครอบครองกิจการแบงก์ 
บาหลีของไอบีอาร์เอ เมื่อปีที่แล้ว เป็นเรื่องที่ถูกต้องและเป็นไปตามกฎหมาย

คำตัดสินชี้ขาดของศาลรัฐบาลจาการ์ตาเมื่อพฤหัสบดีที่แล้ว(30 มี.ค.) สร้างความสับสนให้กับอนาคตของแบงก์ 
บาหลีอย่างมาก และยังไม่ชัดเจนว่า แผนนี้จะส่งผลกระทบต่อธนาคารอื่นๆ ที่ประสบปัญหาทางการเงินและถูกเข้าครอบครองกิจการโดยไอบีอาร์เอ 
หลังจากเกิดวิกฤติการเงินเอเชียช่วงปี 2540-41 ด้วยหรือไม่ โดยที่ปรึกษาด้านกฎหมายของรัฐบาลไม่เปิดเผยชื่อรายหนึ่งกล่าวว่า 
คำชี้ขาดครั้งนี้ก่อให้เกิดคำถามว่า ธนาคารอื่นๆที่ถูกเทคโอเวอร์โดยไอบีอาร์เอและได้รับการตัดสินให้เป็นโมฆะอาจจะยื่นอุทธรณ์ด้วยหรือไม่ 


อย่างไรก็ตาม ที่ปรึกษาด้านกฎหมายประจำไอบีอาร์เอ กล่าวว่า คำตัดสินชี้ขาดไม่ส่งผลกระทบต่อสถานะทางกฎหมายของธนาคารอื่นๆ 
ที่ไอบีอาร์เอเข้าครอบครองกิจการ เพราะคดีนี้เป็นคดีที่เกี่ยวข้องเฉพาะแบงก์ บาหลีเพียงแห่งเดียว

แต่ที่หลายคนปฏิเสธไม่ได้ก็คือ คำตัดสินชี้ขาดของศาลจาการ์ตาครั้งนี้ บั่นทอนอำนาจของไอบีอาร์เอโดยตรง 
โดยเฉพาะในการเร่งปรับโครงสร้างภาคการเงินของประเทศที่ประสบปัญหาอย่างรุนแรง ในช่วงขณะที่รัฐบาลภายใต้การนำของประธานาธิบดีอับดุลเราะห์มาน 
วาฮิด กำลังถูกวิพากษ์วิจารณ์อย่างหนักว่าไม่สามารถทำตามคำมั่นสัญญาที่ให้ไว้ในเรื่องการปฏิรูปเศรษฐกิจ 
และแน่นอนว่า คำตัดสินชี้ขาดของศาลครั้งนี้ ส่งผลกระทบต่อความพยายามของรัฐบาลจาการ์ตาในอันที่จะผลักดันแผนปฏิรูปเศรษฐกิจตามแนวทางที่กองทุนการเงินระหว่างประเทศ 
(ไอเอ็มเอฟ)กำหนดไว้ 

โดยในสัปดาห์ที่ผ่านมา ไอเอ็มเอฟ ชะลอการปล่อยกู้ก้อนใหม่มูลค่า 400 ล้านดอลลาร์แก่อินโดนีเซีย 
เพราะวิตกกังวลปัญหาความล่าช้าในการปฏิรูปเศรษฐกิจ นอกจากนี้ไอเอ็มเอฟ ยังกดดันรัฐบาลจาการ์ตาให้เสริมสร้างความแข็งแกร่งแก่สถาบันการเงินของประเทศทุกแห่งและพยายามปราบปรามการคอรัปชั่นในระบบยุติธรรมด้วย

บรรดานายธนาคารต่างชาติในกรุงจาการ์ตา แสดงทัศนะว่า การตัดสินชี้ขาดครั้งนี้เป็นสัญญาณใหม่ซึ่งเผยให้เห็นปัญหาในระบบยุติธรรมของอินโดนีเซียที่หยั่งรากลึกมานาน 
"การตัดสินครั้งนี้ทำให้เกิดความกลัวขึ้นในจิตใจของบรรดาผู้ปล่อยกู้ต่างชาติอย่างมาก 
เป็นปัญหายักษ์ใหญ่ที่แม้แต่รัฐบาลก็ไม่สามารถเอาชนะได้" นายธนาคารต่างชาติคนหนึ่งกล่าว

ขณะที่บรรดาเจ้าหน้าที่ของไอบีอาร์เอและธนาคารกลางอินโดนีเซีย พยายามลดกระแสความหวาดวิตกอันเกิดจากการตัดสินชี้ขาดของศาล 
พร้อมทั้งยืนยันว่าจะยื่นอุทธรณ์ต่อศาลฎีกา โดยนายอาเมียร์ สยาร์ริฟุดดิน รองผู้อำนวยการฝ่ายกฎหมายของแบงก์ 
อินโดนีเซีย กล่าวว่า แบงก์ บาหลียังคงอยู่ภายใต้การควบคุมดูแลของไอบีอาร์เอในช่วงที่กระบวนการยื่นอุทธรณ์ยังดำเนินอยู่ 
ขณะที่นายสยาริห์ล ซาบิริน ผู้ว่าการธนาคารกลางอินโดนีเซีย กล่าวว่า "สิ่งสำคัญที่สุดในตอนนี้คือลูกค้าต้องไม่เกิดความวิตกกังวล 
รัฐบาลยังคงรับประกันเงินฝากที่บรรดาลูกค้าฝากไว้กับแบงก์ บาหลีต่อไป"

แต่นายเกอร์รี ง้อ รองประธานไอบีอาร์เอซึ่งดูแลการปฏิรูปโครงสร้างภาคธนาคารโดยตรง 
และไม่ได้ปฏิเสธความเป็นไปได้ในการส่งธนาคารบาหลีกลับไปอยู่ภายใต้การบริหารของรูดี 
รามลิ ผู้ก่อตั้ง ทั้งยังเป็นอดีตประธานบริหารแบงก์ บาหลี กล่าวว่าคณะที่ปรึกษาด้านกฎหมายของไอบีอาร์เอกำลังเร่งวิเคราะห์ 
วิจัยคำตัดสินชี้ขาดของศาล 

Krungthep Turakij Newspaper
