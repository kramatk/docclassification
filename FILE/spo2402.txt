ทัพฟ้าดวลโทษดับเจ้าท่า ซิวแชมป์อัมสเทลฟุตซอล

วันจันทร์ ที่ 24 เมษายน พ.ศ.2543
ทัพฟ้าดวลโทษดับเจ้าท่า ซิวแชมป์อัมสเทลฟุตซอล

"จรวดทัพฟ้า" ทหารอากาศ อาศัยความแม่นดวลจุดโทษเชือด "สิงห์เจ้าท่า" ท่าเรือ ไปอย่างตื่นเต้น 
5-4 หลังเจ๊าในเวลา 1-1 พร้อมรับเงินรางวัล 620,000 บาท ขณะที่ ปราจีนบุรี สร้างเซอร์ไพรส์คว้าอันดับ 
3 ด้วยการพิชิต บีอีซี เทโรศาสน 3-2

การแข่งขันฟุตบอล 5 คน ชิงชนะเลิศแห่งประะเทศไทย "อัมสเทล ฟุตซอล 2000" ที่เดอะมอลล์ 
บางกะปิ เมื่อวันที่ 23 เม.ย.ที่ผ่านมา เป็นการฟาดแข้งในรอบรองชนะเลิศ และชิงชนะเลิศ 
โดยในรอบตัดเชือกคู่แรก ทหารอากาศ พบกับ ปราจีนบุรี แม้ว่าทีมจรวดทัพฟ้า จะมีผู้เล่นชื่อชั้นเหนือกว่า 
แต่เกมเป็นไปอย่างสูสี ก่อนที่ทหารอากาศจะใช้ประสบการณ์ที่เหนือกว่าเฉือนเอาชนะไปได้ 
4-3เข้ารอบชิงชนะเลิศเป็นทีมแรก

ขณะที่ในรอบตัดเชือกคู่ที่ 2 "สิงห์เจ้าท่า" การท่าเรือ เต็ง 1 พบกับ บีอีซี เทโรศาสน 
เกมในช่วงแรกทั้งคู่เล่นกันได้อย่างสูสี หมดครึ่งแรกท่าเรือนำ 2-1 แต่ครึ่งหลังทีมจากคลองเตยแก้เกมมาดี 
บดเอาชนะไปได้ 7-2 เข้าไปพบกับ ทหารอากาศในรอบชิงชนะเลิศ 

สำหรับชิงอันดับ 3 ระหว่าง ปราจีนบุรี กับ บีอีซี เทโรศาสน ปรากฏว่า ปราจีนบุรี 
พลิกเอาชนะไปได้ 3-2 ทั้งที่ถูกนำ 0-2 คว้าอันดับ 3 พร้อมเงินรางวัล 80,000 บาทไปครอง

ส่วนรอบชิงชนะเลิศ ทหารอากาศ พบกับ ท่าเรือ เกมในครึ่งแรกสิงห์เจ้าท่าครองเกมเหนือกว่าเล็กน้อย 
แต่ไม่มีทีมใดทำประตูกันได้ จนกระทั่งครึ่งหลัง ท่าเรือ เป็นฝ่ายยิงนำก่อน 1-0 
จากยุทธนา พลศักดิ์ ก่อนที่ วรวิทย์ ถาวรวัน จะมาตีเสมอให้ ทอ.ในอีก 3 นาทีต่อมา 
ทำให้หมดเวลาเสมอกัน 1-1 ต้องดวลจุดโทษตัดสิน ปรากฏว่า ทหารอากาศยิงแม่นกว่าเอาชนะไปได้ 
5-4 คว้าแชมป์ไปครอง พร้อมเงินรางวัล 500,000 บาท ส่วนท่าเรือได้รางวัล 200,000 
บาท

Krungthep Turakij Newspaper
