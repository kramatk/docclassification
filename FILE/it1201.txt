/*****/
ชื่อโดเมนนั้น สำคัญไฉน

ชื่อโดเมนนั้น สำคัญไฉน

ขณะนี้ ไอแคนน์กำลังพิจารณาตั้งชื่อโดเมนใหม่ เพิ่มเติมจากชื่อโดเมนเดิมที่มีอยู่ 
ไม่ว่าจะเป็น ดอทคอม (.com), ดอทเน็ต (.net), ดอทออร์ (.org) โดยเฉพาะดอทคอมนั้น 
ใกล้จะลดจำนวนลงไปทุกทีแล้ว เนื่องจากชื่อโดเมนในสกุลดอทคอม เป็นสกุลที่ได้รับความนิยมมากที่สุด 
ส่งผลให้มีการประมูลซื้อขายกันอย่างคึกคัก ล่าสุด Loans.com เพิ่งขายชื่อโดเมนให้กับแบงก์ออฟอเมริกา 
ไปในราคากว่า 3 ล้านดอลลาร์ 

ชื่อโดเมนมีความสำคัญอย่างไร บรรดาบริษัทอินเทอร์เน็ตจำนวนมาก จึงต้องไขว่คว้าหาชื่อที่จำได้ง่าย 
และมีความหมายสอดคล้องกับธุรกิจของตัวเองมาอยู่ในมือ คำตอบคือ ชื่อโดเมนนั้น เป็นเสมือนทุกสิ่งทุกอย่างสำหรับผู้ที่ต้องการเริ่มต้นทำธุรกิจบนอินเทอร์เน็ต 
นั่นทำให้ทอมดอทคอม (Tom.com) ไซต์ท่าอินเทอร์เน็ตในฮ่องกง ต้องจ่ายเงินถึง 2.5 
ล้านดอลลาร์ เพื่อขอซื้อชื่อโดเมนที่จำง่ายจากบริษัทในสหรัฐมาเป็นของตัวเอง 

มาก่อนได้ก่อน

ขณะที่บริษัทด้านอินเทอร์เน็ตในเอเชีย กำลังให้ความสำคัญกับชื่อโดเมนกันมากขึ้น 
แต่สำหรับบริษัทในสหรัฐแล้ว พวกเขาให้ความสำคัญกับเรื่องนี้มาเป็นเวลานาน ส่งผลให้บริษัทให้คำปรึกษาชื่อโดเมนต่างทยอยกันเข้าสู่ตลาดเอเชีย 
นอกจากนี้ ยังมีโปรแกรมสร้างชื่อโดเมน (name genrator) ซึ่งเป็นโปรแกรมคอมพิวเตอร์ที่ได้รับการพัฒนามา 
เพื่อแนะนำชื่อโดเมน ซึ่งยังไม่ได้รับการจดทะเบียนให้กับผู้ใช้ที่ต้องการจดทะเบียนชื่อโดเมนใหม่ 
ต่างก็ผุดขึ้นมาราวกับดอกเห็ดด้วย 

สำหรับการเข้าไปจดทะเบียนชื่อโดเมนที่สมบูรณ์แบบก่อนที่ใครจะมาฉกไปก่อนนั้น กำลังกลายเป็นปัญหาสำคัญในเอเชียมากขึ้น 
โดยอินเตอร์เน็ต คอร์ปอเรชั่น ฟอร์ แอสไซน์ เนมส์ แอนด์ นัมเบอร์ส หรือไอแคนน์ 
องค์กรระหว่างประเทศที่ดูแลด้านที่อยู่อินเทอร์เน็ต รายงานว่า ขณะนี้ ชื่อโดเมนดอทคอม, 
ดอทเน็ต , ดอทออร์ จำนวนราว 13.4 ล้านชื่อได้รับการจดทะเบียนเรียบร้อยแล้ว และทุกวันจะมีการจดชื่อโดเมนกว่า 
10,000 ชื่อ ผ่านทางบริษัทรับจดทะเบียนชื่อโดเมนราว 110 แห่งทั่วโลก 

ด้วยเหตุนี้ ทำให้มีกลุ่มผู้ฉกฉวยโอกาส โดยเฉพาะผู้ที่อยู่ในประเทศ "ศิวิไลซ์" 
ทั้งหลาย เริ่มแสวงหาผลประโยชน์จากผู้ด้อยโอกาสกว่า โดยเฉพาะจากบริษัทหน้าใหม่ 
ที่เพิ่งจะก้าวเข้าสู่โลกการค้าบนอินเทอร์เน็ต 

ประมูลชื่อโดเมนชาติเอเชีย

 กรณีตัวอย่างที่น่าจะนำมาสนับสนุนคำกล่าวข้างต้นได้เป็นอย่างดี น่าจะเป็นเรื่อง 
"การเปิดประมูลชื่อโดเมนรัฐบาลเอเชีย" เหตุการณ์นี้เกิดขึ้นที่สิงคโปร์ โดยหนังสือพิมพ์สเตรตส์ 
ไทมส์ รายงานว่า ชื่อโดเมนอินเทอร์เน็ตของรัฐบาลประเทศต่างๆ ในเอเชีย ซึ่งได้รับการจดทะเบียนในอังกฤษ 
กำลังถูกเสนอขายให้แก่ผู้ที่ให้ราคาประมูลสูงสุด 

หนังสือพิมพ์ท้องถิ่นฉบับนี้ แจ้งว่า ชายชาวอังกฤษคนหนึ่งได้จดทะเบียนชื่อ www.GovernmentofSingapore.com 
และชื่อโดเมนอื่นในลักษณะคล้ายกันนี้ ของมาเลเซีย, จีน, อินโดนีเซีย, บรูไน และไทย 
และเขาก็เต็มใจที่จะขายสิทธิในชื่อโดเมนนี้ ให้กับใครก็ได้ ที่เต็มใจสู้ราคา 

ดังนั้น "หากคุณคิดว่า ณ ขณะนี้ ยังไม่พร้อมจะทำธุรกิจบนอินเทอร์เน็ต แต่คาดว่าในอนาคตจะต้องทำแน่นอน 
อย่างแรกที่คุณต้องทำคือ เข้าไปจดชื่อโดเมนที่คุณคิดว่าน่าจะใช้ได้เอาไว้ก่อน 
โดยค่าธรรมเนียมในการจดชื่อโดเมนต่อปีนั้น ส่วนใหญ่จะมีราคาระหว่าง 25-75 ดอลลาร์ 
และอาจต้องเสียค่าบำรุงอีกเล็กน้อยในปีต่อๆ ไป ซึ่งนับว่าคุ้ม" นายสตีฟ ริฟคิน 
ที่ปรึกษาด้านชื่อโดเมนในนิวเจอร์ซี และเป็นคอลัมนิสต์ให้กับหนังสือพิมพ์ เอเชีย 
แบรนด์ นิวส์ ในฮ่องกงกล่าว 

ความหวังชื่อโดเมนใหม่

สำหรับการเปิดตัวชื่อโดเมนใหม่ของไอแคนน์ ซึ่งใช้เวลาพิจารณากว่า 1 ปีแล้ว แต่ยังไม่คืบหน้ามากนัก 
เนื่องจากยังติดปัญหาเรื่องการละเมิดลิขสิทธิ์ ของเจ้าของเครื่องหมายการค้า และการตีความในเชิงการค้าอื่นๆ 
โดยคณะกรรมการในไอแคนน์ ให้ความเห็นว่า ต้องมีการออกมาตรการป้องกันทรัพย์สินทางปัญญา 
ด้านเครื่องหมายการค้าให้เพียงพอก่อนที่จะนำชื่อโดเมนใหม่มาใช้ 

กระนั้นบรรดาองค์กรธุรกิจต่างพากันกังวลว่า หากมีชื่อโดเมนใหม่เกิดขึ้น ก็จะต้องมีรอบใหม่ของแก๊งลักลอบจดชื่อโดเมนเกิดขึ้นแน่นอน 
และเจ้าของเครื่องหมายการค้า จะต้องออกไปสู่คดีในชั้นศาลเช่นเดิม ซึ่งในที่นี่หมายรวมไปถึงศาลทั่วโลกด้วย 


สหรัฐอนุมัติกฎหมายชื่อโดเมน

อย่างไรก็ตาม เรื่องดังกล่าวก็ไม่ได้ทำให้สังคมโลกนิ่งนอนใจ โดยเฉพาะสหรัฐที่ขณะนี้สมาชิกสภาผู้แทนราษฎรสหรัฐผ่านกฎหมาย 
ที่จะทำให้การประมูลซื้อขายชื่อโดเมนที่เป็นเครื่องหมายการค้า หรือเครื่องหมายจดทะเบียนสำหรับบริการใดบริการหนึ่งของบริษัท 
ที่เกิดจากกลุ่มฉวยโอกาสเอาชื่อเหล่านี้ ไปจดชื่อโดเมนอินเทอร์เน็ต แล้วนำออกประมูลขาย 


สำหรับกฎหมายดังกล่าว จะห้ามการจดทะเบียน, ขาย หรือใช้ชื่อโดเมน ที่เหมือน หรือคล้ายจนอาจทำให้เกิดความสับสน 
กับเครื่องหมายการค้าที่มีเจ้าของแล้ว ณ เวลาที่ไปขอจดทะเบียน โดยผู้ฝ่าฝืนจะถูกปรับอย่างสูงไม่เกิน 
100,000 ดอลลาร์ และสำหรับไซต์ที่จดทะเบียนไปก่อนหน้าที่กฎหมายจะมีผลใช้บังคับ 
ให้ผู้เป็นเจ้าของเครื่องหมายการค้า สามารถเรียกร้องให้มีการยกเลิก หรือคืนชื่อโดเมน 
ที่ใช้ชื่อเครื่องหมายการค้า หรือชื่ออันเป็นที่รู้จักแพร่หลายของบริษัทได้ 

นอกจากนี้ กฎหมายฉบับนี้ ยังครอบคลุมไปถึงบริษัทต่างชาติ ที่จดทะเบียนชื่อโดเมน 
สามารถถูกฟ้องในศาลสหรัฐได้ หากพวกเขาไปจดทะเบียนชื่อโดเมน ที่เป็นเครื่องหมายการค้าของผู้อื่นในสหรัฐ 
ซึ่งก็มีนักวิจารณ์บอกว่า มาตรานี้อาจนำไปบังคับใช้ไม่ง่ายนัก 

KT Internet Dept


Home I The Nation I EntNet I เนชั่นสุดสัปดาห์ I วิทยุเนชั่น
About Us I FAQ I Site Map I Contact Us I Policy
