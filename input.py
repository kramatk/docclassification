# -*- coding: utf-8 -*-
from kucut import SimpleKucutWrapper as KUCut
from os import listdir
from os.path import isfile, join
import sys
from stopword import stopword,cutword
import math

class Data:
	def __init__(self):
		self.N = 0
		self.df = {}

	def get_class(self,file_dir):
		if file_dir[0]=="e":
			return "edu"
		elif file_dir[0]=="f":
			return "for"
		elif file_dir[0]=="i":
			return "it"
		elif file_dir[0]=="p":
			return "pol"
		elif file_dir[0]=="s":
			return "spo"
		else:
			return "non"


	def get_class_list(self):
		return ["edu","for","it","pol","spo","non"]

	def idf(self,w):
		if self.df.has_key(w):
			#print self.N*1.0/self.df[w]
			return math.log10(self.N*1.0/self.df[w])
		else:
			return 1



	def prepare_word(self,line):
		#sp = line.split(" ")
		sp = cutword(line)
		#sp = stopword(sp)
		return sp

	def query(self,path):
		f = open(path)
		freq = {}
		for line in f:
			sp = self.prepare_word(line)	
			for w in sp:				
				if freq.has_key(w):
					freq[w]+=1
				else:
					freq[w]=1
		return freq

	def input_file(self,mypath):
		#mypath = sys.argv[1]
		files = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
		files.sort()
		#myKUCut = KUCut()

		data = {"edu":[],"for":[],"it":[],"pol":[],"spo":[],"non":[]}
		
		for file_dir in files:
			
			if "cut" not in file_dir:
				continue

			self.N += 1				
			#print  file_dir," --------- "
			path = mypath +file_dir
			f = open(path)
			file_class = self.get_class(file_dir)

			freq = {}
			for line in f:
				sp = self.prepare_word(line)
				for w in sp:				
					if freq.has_key(w):
						freq[w]+=1
					else:
						freq[w]=1

						### df ###
						if self.df.has_key(w):
							self.df[w]+=1
						else:
							self.df[w]=1

			data[file_class].append(freq)		
			#print path	
		return data

